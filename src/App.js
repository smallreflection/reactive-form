import React, {createContext, useState, useEffect, useContext, useCallback, useReducer} from 'react';

import './App.css';
import _ from 'lodash';
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// https://stackoverflow.com/questions/55757761/handle-an-input-with-react-hooks

// https://upmostly.com/tutorials/how-to-use-the-usecontext-hook-in-react

var dataStore = {
  people: [
		 {
      id: 1,
			firstName: 'Jim',
			lastName: 'Smith',
			email: 'jims@email.com',
			age: 32,
			job: 'Engineer',
			transportation: 'car'
		 },
		 {
      id: 2,
      firstName: 'Sarah',
      lastName: 'Alton',
      email: 'salton@email.com',
      age: 24,
      job: 'Programmer',
      transportation: 'bike'
    },
		{
      id: 3,
      firstName: 'Leslie',
      lastName: 'Leeland',
      email: 'lleeland@woohoo.com',
      age: 24,
      job: 'Teacher',
      transportation: 'Scooter'
    }
	]
};

document.title = "Reactive Form";


// define that context takes an object and an update function
const PeopleContext = createContext(dataStore.people);

const PeopleContextProvider = (props) => {
	// dataStore.people as default context state, with a reducer dispatch returned for use with useContext hook to add a new person from the form
	// note you cannot push to a state array because this mutates state directly
  // https://medium.com/javascript-in-plain-english/how-to-add-to-an-array-in-react-state-3d08ddb2e1dc
  
  const peopleStore = dataStore.people;

  const [peopleStoreContext, action] = useReducer(PeopleContextReducer, peopleStore);
  
  // define the reducer function - state is peopleStore by default
  function PeopleContextReducer(state, action) {
    switch (action.type) {
      case "addUser":
        var currentLength = state.length + 1;
        return state.concat({id: currentLength++, ...action.payload});
      case "removeUser":
        // const index = _.findIndex(state, {id: action.payload})
        // state.splice(index, 1);
        // return state
        return state.filter(el => !(el.id === action.payload));
      default:
        throw new Error();
    }
  } 

	return(
		// pass reducer state and callback action as single array value to the context
		<PeopleContext.Provider value={[peopleStoreContext, action]}>
			{props.children}
		</PeopleContext.Provider>
	)
} // ------------------------ People Context


function RecentPeople() {
	// peopleStore equivalent to value prop of PeopleContext provider
	const [peopleStoreContext, dispatch] = useContext(PeopleContext);

  const memoizedRenderPeopleNameList = useCallback(
    () => {
      return(<>{_.map(peopleStoreContext, ({id, firstName, lastName}) => 
        <li key={id}>
          <span className="user">{id}: {firstName} {lastName}</span>
          <button 
            onClick={() => dispatch({type: 'removeUser', payload: id})} 
            className="link"><FontAwesomeIcon icon={faTrashAlt} /> Remove
          </button>
        </li>)}</>
      );
    }, [peopleStoreContext, dispatch])
  
    // had to use brackets here
	  useEffect(() => { memoizedRenderPeopleNameList() }, [peopleStoreContext, memoizedRenderPeopleNameList]);
  
    // console.log('render');

	  return(
      <>
      <h3>Recent Users {peopleStoreContext.length}</h3>
      <ul className="people-name-list">
        {memoizedRenderPeopleNameList()}
      </ul>
      </>
	);
} // ------------------------ end RecentPeople

function Input(props) {
	let {inputId, value, handleChange } = props;
	let inputLabel = _.startCase(props.inputId);
	return(
		<div className="input-group">
			<label>{inputLabel}</label>
			<input id={inputId} value={value} name={inputId} onChange={handleChange} />
		</div>
	);
};
	
const inputFields = [ 'firstName', 'lastName', 'email', 'age', 'job', 'transportation' ]

const initialFormState = {
	firstName: '',
	lastName: '',
	email: '',
	age: null,
	job: '',
	transportation: ''
};


function NewUserForm(props) {
	// pass in the reducer function inline to concat existing and new state
  const [inputValues, setInputValues] = useReducer((state, newState) => ({...state, ...newState}), initialFormState);
  
  const [formDisabled, setFormDisabled] = useState(false);
	
	const [peopleStoreContext, dispatch] = useContext(PeopleContext);
	
	console.log(peopleStoreContext); // log initial context
		
	function handleOnChange(event) {
	 const {name, value} = event.target;
			// call the reducer update function, passing in existing formstate with state of current input
			setInputValues({...inputValues, [name]: value });
	}
  
  useEffect(() => {
    return (inputValues.firstName.length > 0 && inputValues.lastName.length > 0) ? 
      setFormDisabled(false) : setFormDisabled(true) }, 
  [inputValues]);

	function handleSubmit(event) {
		dispatch({type: 'addUser', payload: inputValues});
		console.log(peopleStoreContext); // log updated context
    setInputValues(initialFormState);
    event.preventDefault();
	}
	
	// don't pass event here
	function handleReset() {
		setInputValues(initialFormState);
	}
	
	return(
		<form id="new-user-form" onSubmit={() => handleSubmit}>
			{_.map(inputFields, (fieldName) => (<Input key={fieldName} type="text" name={fieldName} inputId={fieldName} handleChange={handleOnChange} />))}
			
			<button type="submit" value="submit" disabled={formDisabled} onClick={handleSubmit}>Add User</button>
			
			<button type="reset" style={{"marginLeft": "20px"}} onClick={handleReset}>Reset Form</button>
		</form>
	);
} // form


function App() {
	return(
		<PeopleContextProvider>
		<div id="main">
			<h1>Add a User!</h1>
			<NewUserForm />
			<RecentPeople />
		</div>
		</PeopleContextProvider>
	);
};

export default App;
